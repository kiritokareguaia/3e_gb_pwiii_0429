import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadImmbiliariaRoutingModule } from './cad-immbiliaria-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadImmbiliariaRoutingModule
  ]
})
export class CadImmbiliariaModule { }
