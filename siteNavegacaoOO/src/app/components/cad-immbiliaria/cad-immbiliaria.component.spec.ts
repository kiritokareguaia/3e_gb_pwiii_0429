import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadImmbiliariaComponent } from './cad-immbiliaria.component';

describe('CadImmbiliariaComponent', () => {
  let component: CadImmbiliariaComponent;
  let fixture: ComponentFixture<CadImmbiliariaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadImmbiliariaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadImmbiliariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
