import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadProprietarioRoutingModule } from './cad-proprietario-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadProprietarioRoutingModule
  ]
})
export class CadProprietarioModule { }
