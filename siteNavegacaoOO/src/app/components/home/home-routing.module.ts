import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadImmbiliariaComponent } from '../cad-immbiliaria/cad-immbiliaria.component';
import { CadImovelComponent } from '../cad-imovel/cad-imovel.component';
import { CadLocadorComponent } from '../cad-locador/cad-locador.component';
import { CadProprietarioComponent } from '../cad-proprietario/cad-proprietario.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
