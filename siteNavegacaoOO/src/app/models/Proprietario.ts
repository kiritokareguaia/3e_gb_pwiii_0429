export class Proprietario {
    private _nome: string;
    private _telefone: string;
    private _dataNascimento: Date;
    private _sexo: string;
    private _fone: string;
    private _email: string;
    constructor(nome:string, telefone:string, dataNascimento:Date, sexo:string, fone:string, email:string)
    {
        this._dataNascimento=dataNascimento;
        this._email=email;
        this._fone=fone;
        this._nome=nome;
        this._sexo=sexo;
        this._telefone=telefone;
    }
    public set nome(nome: string){

        this._nome = nome

    }
    public set telefone(telefone: string){

        this._telefone = telefone

      }

      public get telefone(): string{

        return this._telefone

     }
    public set dataNascimento(dataNascimento: Date){

        this._dataNascimento = dataNascimento

      }

      public get dataNascimento(): Date{

        return this._dataNascimento

     }



     public set sexo(sexo: string){

        this._sexo = sexo

      } public get sexo(): string{

        return this._sexo

     }
     public set fone(fone: string){

        this._fone = fone}

      public get efone(): string{

        return this._fone
     }

}